package app

import "flag"

type Flags struct {
	configPath *string
	logMode    *string
}

//Получение флагов из консоли
func getFlags() (*Flags, error) {
	f := new(Flags)
	f.configPath = flag.String("cfg", "config", "Path to config file")
	f.logMode = flag.String("log", "prod", "Log (dev|prod)")
	flag.Parse()

	return f, nil
}
