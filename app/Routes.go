package app

import (
	"bitbucket.org/dolnikov-v/go-lesson/controllers"
)

func initRoutes(app *Application) {
	baseController := controllers.NewBaseController()
	languageController := controllers.NewLanguageController(app.Config, app.LanguageFacade)
	userController := controllers.NewUserController(app.Config, app.UserFacade)

	routes := []HttpRoute{
		//Запрос на получение статуса сервера
		//Метод OPTIONS вызывается перед выполением любого запрса
		//Чтобы api клиент мог убедиться что сервер доступен
		{
			Path:    "/*filepath",
			Type:    RouteTypeOptions,
			Handler: baseController.HandleOption,
			Private: false,
		},
		//Регистарция пользователя
		{
			Path:    "/user",
			Type:    RouteTypePost,
			Handler: userController.HandlePost,
			Private: false,
		},
		//Удаление пользователя
		{
			Path:    "/user",
			Type:    RouteTypeDelete,
			Handler: userController.HandleDelete,
			Private: true,
		},
		//Получение информации о пользователе
		{
			Path:    "/user/info/email/:email",
			Type:    RouteTypeGet,
			Handler: userController.HandleGetUserInfoByEmail,
			Private: true,
		},
		//Проверяем существет ли пользователь по ID
		{
			Path:    "/user/exist/id/:id",
			Type:    RouteTypeGet,
			Handler: userController.HandleGetExistById,
			Private: false,
		},
		//Проверяем существет ли пользователь по Email
		{
			Path:    "/user/exist/email/:email",
			Type:    RouteTypeGet,
			Handler: userController.HandleGetExistByEmail,
			Private: false,
		},
		//Проверяем логин пароль пользователя
		{
			Path:    "/user/login",
			Type:    RouteTypePost,
			Handler: userController.HandleLoginPost,
			Private: false,
		},
		//Проверяем логин пароль пользователя
		{
			Path:    "/user/login/refresh",
			Type:    RouteTypePost,
			Handler: userController.HandleLoginRefreshPost,
			Private: false,
		},
		//Получение списка языков
		{
			Path:    "/language",
			Type:    RouteTypeGet,
			Handler: languageController.HandleGet,
			Private: false,
		},
	}

	app.HttpInstance.setRoutes(&routes)
}
