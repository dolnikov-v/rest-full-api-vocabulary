package app

import (
	"bitbucket.org/dolnikov-v/go-lesson/app/config"
	"bitbucket.org/dolnikov-v/go-lesson/app/database/postgres"
	"bitbucket.org/dolnikov-v/go-lesson/facades"
	"bitbucket.org/dolnikov-v/go-lesson/helpers"
	"database/sql"
	"github.com/rs/zerolog"
	"strconv"
)

type Application struct {
	Config         *config.Config
	Logger         *zerolog.Logger
	PostgresClient *sql.DB
	HttpInstance   *HttpServerInstance
	LanguageFacade *facades.LanguageFacade
	UserFacade     *facades.UserFacade
}

func NewApplication() (*Application, error) {

	//Инициализация флагов из консоли
	flags, err := getFlags()
	if err != nil {
		return nil, err
	}

	//Подключение конфиг файла
	configData, err := config.GetConfigFromFile(*flags.configPath + ".toml")
	if err != nil {
		return nil, err
	}

	//Инициализация SSH ключей
	err = Helper.InitSshKeys()
	if err != nil {
		return nil, err
	}

	//Инициализация системы логирования
	logger, err := InitLogger(config.AppName, *flags.logMode)
	if err != nil {
		return nil, err
	}

	//Подключение к базе Postgres
	postgresClient, err := postgres.NewClient(configData.PostgresSettings)
	if err != nil {
		return nil, err
	}

	//Инициализация сервера
	e := newHttpServerInstance(configData)
	e.http.Logger = &logger
	go func() {
		err := e.Start(":" + strconv.FormatInt(configData.SystemSettings.Port, 10))
		if err != nil {
			panic(err)
		}
	}()

	//Инициализация фасада language
	lf := facades.NewLanguageFacade(postgresClient)
	uf := facades.NewUserFacade(postgresClient)

	//Сборка приложения
	app := &Application{
		Config:         configData,
		Logger:         &logger,
		PostgresClient: postgresClient,
		HttpInstance:   &e,
		UserFacade:     uf,
		LanguageFacade: lf,
	}

	//Инициализация рутов
	initRoutes(app)
	return app, nil
}
