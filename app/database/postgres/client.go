package postgres

import (
	"bitbucket.org/dolnikov-v/go-lesson/app/config"
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"github.com/pkg/errors"
)

type DBConnection interface {
	Query(query string, args ...interface{}) (*sql.Rows, error)
	Exec(query string, args ...interface{}) (sql.Result, error)
	QueryRow(query string, args ...interface{}) *sql.Row
}

func NewClient(c *config.PostgresSettings) (*sql.DB, error) {
	connectionString := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=%s search_path=%s", c.Host, c.Port, c.User, c.Password, c.Database, c.SslMode, c.Scheme)
	client, err := sql.Open(c.Driver, connectionString)
	if err != nil {
		panic(err)
	}

	err = client.Ping()
	if err != nil {
		return nil, errors.Wrap(err, "Postgres client creation:")
	}
	return client, nil
}
