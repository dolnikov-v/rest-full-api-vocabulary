package app

import (
	"bitbucket.org/dolnikov-v/go-lesson/app/config"
	Helper "bitbucket.org/dolnikov-v/go-lesson/helpers"
	"fmt"
	"github.com/fasthttp/router"
	"github.com/valyala/fasthttp"
	"log"
	"os"
	"runtime/debug"
	"time"
)

type (
	//EchoInstance Echo adapter
	HttpServerInstance struct {
		http *fasthttp.Server
	}
	//EchoRoute Route config
	HttpRoute struct {
		Path    string
		Handler func(ctx *fasthttp.RequestCtx)
		Type    string
		Private bool
	}
)

const (
	RouteTypePost    = "post"   // POST — создание ресурса
	RouteTypeGet     = "get"    // GET — получение информации о ресурсе
	RouteTypeDelete  = "delete" // DELETE — удаление ресурса
	RouteTypePut     = "put"    // PUT — замена ресурса целиком
	RouteTypePatch   = "patch"  // PATCH — редактирование ресурса частично
	RouteTypeOptions = "options"
)

func newHttpServerInstance(c *config.Config) HttpServerInstance {
	e := &fasthttp.Server{
		Name:                               config.ServerVersion,
		Concurrency:                        c.FastHttpSettings.Concurrency,
		DisableKeepalive:                   c.FastHttpSettings.DisableKeepalive,
		ReadBufferSize:                     c.FastHttpSettings.ReadBufferSize,
		WriteBufferSize:                    c.FastHttpSettings.WriteBufferSize,
		ReadTimeout:                        time.Duration(c.FastHttpSettings.ReadTimeout) * time.Second,
		WriteTimeout:                       time.Duration(c.FastHttpSettings.WriteTimeout) * time.Second,
		MaxConnsPerIP:                      c.FastHttpSettings.MaxConnsPerIP,
		MaxRequestsPerConn:                 c.FastHttpSettings.MaxRequestsPerConn,
		MaxKeepaliveDuration:               time.Duration(c.FastHttpSettings.MaxKeepaliveDuration) * time.Second,
		TCPKeepalive:                       c.FastHttpSettings.TCPKeepalive,
		TCPKeepalivePeriod:                 time.Duration(c.FastHttpSettings.TCPKeepalivePeriod) * time.Second,
		MaxRequestBodySize:                 c.FastHttpSettings.MaxRequestBodySize,
		ReduceMemoryUsage:                  c.FastHttpSettings.ReduceMemoryUsage,
		GetOnly:                            c.FastHttpSettings.GetOnly,
		LogAllErrors:                       c.FastHttpSettings.LogAllErrors,
		DisableHeaderNamesNormalizing:      c.FastHttpSettings.DisableHeaderNamesNormalizing,
		SleepWhenConcurrencyLimitsExceeded: time.Duration(c.FastHttpSettings.SleepWhenConcLimitsExceeded) * time.Second,
		NoDefaultServerHeader:              c.FastHttpSettings.NoDefaultServerHeader,
		NoDefaultContentType:               c.FastHttpSettings.NoDefaultContentType,
	}

	return HttpServerInstance{
		http: e,
	}
}

func (e *HttpServerInstance) setRoutes(routes *[]HttpRoute) {
	serviceRouter := router.New()
	w := os.Stdout
	log.SetOutput(w)
	logsCh := make(chan string)

	go func() {
		for {
			log.Println(<-logsCh)
		}
	}()

	requestMiddleware := func(ctx *fasthttp.RequestCtx, r HttpRoute) {
		defer func() {
			ctx.Response.Header.Set("Access-Control-Allow-Methods", "GET,POST,PUT,PATCH,DELETE,OPTIONS")
			ctx.Response.Header.Set("Access-Control-Allow-Origin", "*")
			ctx.Response.Header.Set("Access-Control-Allow-Headers", "Content-Type")
			ctx.Response.Header.Set("Access-Control-Allow-Credentials", "true")

			if r := recover(); r != nil {
				debug.PrintStack()
				ctx.Response.SetStatusCode(500)
				ctx.Response.SetBodyString(fmt.Sprint(r))
				go log.Printf("Fatal: %s", r)
			} else {
				ctx.SetContentType("application/json")
			}

			if r.Type != RouteTypeOptions {
				logsCh <- fmt.Sprintf("Response %d: %s", ctx.Response.StatusCode(), ctx.Response.Body())
			}

		}()

		if r.Private {
			authToken := string(ctx.Request.Header.Peek("Authorization"))
			isAuth := Helper.CheckJWToken(authToken)

			if !isAuth {
				ctx.Response.SetStatusCode(401)
				ctx.Response.SetBodyString(fmt.Sprint("Unauthorized! This method requires authorization"))
				return
			}
		}

		if r.Type != RouteTypeOptions {
			uri := ctx.Request.URI().String()
			logsCh <- fmt.Sprintf("Request: %s %s", uri, ctx.Request.Body())
			r.Handler(ctx)
		}
	}

	for _, route := range *routes {
		r := route
		switch route.Type {
		case RouteTypeGet:
			serviceRouter.GET("/v1"+route.Path, func(ctx *fasthttp.RequestCtx) {
				requestMiddleware(ctx, r)
			})
		case RouteTypeDelete:
			serviceRouter.DELETE("/v1"+route.Path, func(ctx *fasthttp.RequestCtx) {
				requestMiddleware(ctx, r)
			})
		case RouteTypePost:
			serviceRouter.POST("/v1"+route.Path, func(ctx *fasthttp.RequestCtx) {
				requestMiddleware(ctx, r)
			})
		case RouteTypePut:
			serviceRouter.PUT("/v1"+route.Path, func(ctx *fasthttp.RequestCtx) {
				requestMiddleware(ctx, r)
			})
		case RouteTypePatch:
			serviceRouter.PATCH("/v1"+route.Path, func(ctx *fasthttp.RequestCtx) {
				requestMiddleware(ctx, r)
			})
		case RouteTypeOptions:
			serviceRouter.OPTIONS("/v1"+route.Path, func(ctx *fasthttp.RequestCtx) {
				requestMiddleware(ctx, r)
			})
		}
	}

	e.http.Handler = serviceRouter.Handler
}

//Запуск сервера
func (e *HttpServerInstance) Start(s string) interface{} {
	return e.http.ListenAndServe(s)
}
