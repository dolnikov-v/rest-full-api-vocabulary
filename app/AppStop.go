package app

import (
	"bitbucket.org/dolnikov-v/go-lesson/app/config"
	"context"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"
)

//ЗАВЕРШЕНИЕ ПРИЛОЖЕНИЯ
func (app *Application) StopApplication() {
	quit := make(chan os.Signal)
	wait := make(chan bool)
	signal.Notify(quit, syscall.SIGTERM, syscall.SIGINT)
	log.Println("Для остановки нажмите CTRL + C")
	<-quit
	log.Println("Завершение работы приложения:")
	ctx, _ := context.WithTimeout(context.Background(), config.ServiceStoppingTimeout*time.Second)

	//Если приложение не успело осановться за указнное в ServiceStoppingTimeout время
	go func() {
		select {
		case <-ctx.Done():
			log.Printf("Не удалось изящно остановить приложение, истекло время ожидания (ServiceStoppingTimeout = %dc)", config.ServiceStoppingTimeout)
			wait <- true
		}
	}()

	//Процедура останоки приложения
	go func() {
		if err := app.Stop(ctx); err != nil {
			log.Printf("Ошибка остановки приложения: %s", err.Error())
		} else {
			log.Println("Приложение успешно остановлено!")
		}
		wait <- true
	}()
	<-wait
}

//Остановка сервисов
func (app *Application) Stop(ctx context.Context) error {

	//Отсановка http сервера
	err := app.HttpInstance.http.Shutdown()
	if err != nil {
		log.Printf("ОШИБКА [Http Server]: %s", err)
	} else {
		log.Println("ГОТОВО [Http Server]")
	}

	//Отсановка postgres
	err = app.PostgresClient.Close()
	if err != nil {
		log.Printf("ОШИБКА [PostgreSQL]: %s", err)
	} else {
		log.Println("ГОТОВО [PostgreSQL]")
	}

	return nil
}
