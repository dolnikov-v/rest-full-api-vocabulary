package app

import (
	"github.com/rs/zerolog"
	"log"
	"os"
	"time"
)

func InitLogger(LogName string, logMode string) (logger zerolog.Logger, err error) {
	zerolog.TimeFieldFormat = time.RFC1123
	zerolog.DurationFieldUnit = time.Millisecond
	zerolog.DurationFieldInteger = true
	_ = LogName

	if logMode == "dev" {
		log.Println("Включен метод логирования:", logMode)
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
		logger = zerolog.New(os.Stdout).With().Timestamp().Logger()
		return logger, nil
	} else if logMode == "prod" {
		log.Println("Приложение запущено без логирования")
		return
	}

	log.Println("Приложение запущено без логирования!!! Неправильный метод логирования:", logMode)
	return
}
