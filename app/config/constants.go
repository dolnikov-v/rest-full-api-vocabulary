package config

//Системные константы
const (
	AppName                = "vocabulary"
	ServerVersion          = "Vocabulary/1.0.0"
	ServiceStoppingTimeout = 90
)

//Константы называний переменных в вызываемых методах
const (
	AttributeId    = "id"
	AttributeEmail = "email"
)

//Path to Ssh keys
const (
	PrivateKeyPath = "app/config/keys/auth.rsa.key"
	PublicKeyPath  = "app/config/keys/auth.rsa.key.pub"
)
