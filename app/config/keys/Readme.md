Add ssh keys to this folder. With RSA PEM format

1) /app/config/keys/auth.rsa.key (Для сервера авторизации)
2) /app/config/keys/auth.rsa.key.pub (Для проверки токена)

---

HOW TO:  
https://gist.github.com/ygotthilf/baa58da5c3dd1f69fae9

>ssh-keygen -t rsa -b 2048 -m PEM -f auth.rsa.key
>
>OR
>
>ssh-keygen -t rsa -b 4096 -m PEM -f auth.rsa.key

Don't add passphrase
 
>openssl rsa -in auth.rsa.key -pubout -outform PEM -out auth.rsa.key.pub