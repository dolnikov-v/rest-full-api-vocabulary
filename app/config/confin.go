package config

import (
	"github.com/pelletier/go-toml"
	"github.com/pkg/errors"
	"io/ioutil"
	"os"
	"reflect"
)

type Config struct {
	SystemSettings   *SystemSettings   `toml:"system_settings"`
	FastHttpSettings *FastHttpSettings `toml:"fast_http_settings"`
	PostgresSettings *PostgresSettings `toml:"postgres_connection_settings"`
}

type SystemSettings struct {
	Host string `toml:"host"`
	Port int64  `toml:"port"`
}

type FastHttpSettings struct {
	Concurrency                   int  `toml:"concurrency"`
	DisableKeepalive              bool `toml:"disable_keepalive"`
	ReadBufferSize                int  `toml:"read_buffer_size"`
	WriteBufferSize               int  `toml:"write_buffer_size"`
	ReadTimeout                   int  `toml:"read_timeout"`
	WriteTimeout                  int  `toml:"write_timeout"`
	MaxConnsPerIP                 int  `toml:"max_connections_per_ip"`
	MaxRequestsPerConn            int  `toml:"max_requests_per_connection"`
	MaxKeepaliveDuration          int  `toml:"max_keepalive_duration"`
	TCPKeepalive                  bool `toml:"tcp_keepalive"`
	TCPKeepalivePeriod            int  `toml:"tcp_keepalive_period"`
	MaxRequestBodySize            int  `toml:"max_request_body_size"`
	ReduceMemoryUsage             bool `toml:"reduce_memory_usage"`
	GetOnly                       bool `toml:"get_only"`
	LogAllErrors                  bool `toml:"log_all_errors"`
	DisableHeaderNamesNormalizing bool `toml:"disable_header_names_normalizing"`
	SleepWhenConcLimitsExceeded   int  `toml:"sleep_when_concurrency_limit_exceeded"`
	NoDefaultServerHeader         bool `toml:"no_default_server_header"`
	NoDefaultContentType          bool `toml:"no_default_content_type"`
}

type PostgresSettings struct {
	Driver   string `toml:"driver"`
	User     string `toml:"user"`
	Password string `toml:"password"`
	Host     string `toml:"host"`
	Port     string `toml:"port"`
	Database string `toml:"database"`
	SslMode  string `toml:"sslmode"`
	Scheme   string `toml:"scheme"`
}

// Получение покфига из файла
func GetConfigFromFile(filePath string) (*Config, error) {
	f, err := os.Open(filePath)
	if err != nil {
		return nil, err
	}

	b, err := ioutil.ReadAll(f)
	if err != nil {
		return nil, err
	}

	cfg := &Config{}
	if err = toml.Unmarshal(b, cfg); err != nil {
		return nil, err
	}

	err = cfg.checkConfig()
	if err != nil {
		return nil, err
	}

	return cfg, nil
}

func (c *Config) checkConfig() error {
	value := reflect.ValueOf(*c)
	for i := 0; i < value.NumField(); i++ {
		if value.Field(i).IsNil() {
			return errors.Errorf("%s", value.Field(i).Type())
		}
	}
	return nil
}
