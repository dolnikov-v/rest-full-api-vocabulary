package repositories

import "bitbucket.org/dolnikov-v/go-lesson/models"

type UserRepository interface {
	Save(user *models.SqlUser) (*models.SqlUser, error)
	DeleteByUserId(user *models.DeleteUserRequest) error
	SelectUser(user *models.GetUserInfo) error
	IsUserExistById(id int) bool
	IsUserExistByEmail(id string) bool
	SelectUserAuthorizationData(user *models.GetAccessInfo) error
}
