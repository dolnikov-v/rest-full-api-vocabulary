package repositories

import (
	"bitbucket.org/dolnikov-v/go-lesson/app/database/postgres"
	"bitbucket.org/dolnikov-v/go-lesson/models"
	"strconv"
	"strings"
)

type postgresUserRepository struct {
	client postgres.DBConnection
}

func NewUserRepository(db postgres.DBConnection) UserRepository {
	ur := postgresUserRepository{
		client: db,
	}
	return &ur
}

func (ur *postgresUserRepository) Save(u *models.SqlUser) (*models.SqlUser, error) {
	err := ur.insert(u)
	if err != nil {
		return nil, err
	}
	return u, nil
	//Тут же можно добавить редактрование
}

func (ur *postgresUserRepository) insert(u *models.SqlUser) error {

	/*NEED CRITICAL REFACTORING*/
	/*Нужно добавить проверку существует ли уже клиент*/
	query := `INSERT INTO "user" (email, password, native_language, created_at, updated_at, deleted) VALUES ($1, $2, $3, $4, $5, $6) RETURNING id`

	rows, err := ur.client.Query(query, &u.Email, &u.Password, &u.NativeLanguage, &u.CreatedAt, &u.UpdatedAt, &u.Deleted)
	if err != nil {
		return err
	}

	defer rows.Close()

	rows.Next()
	err = rows.Scan(&u.Id)
	if err != nil {
		return err
	}

	return nil
}

func (ur *postgresUserRepository) DeleteByUserId(u *models.DeleteUserRequest) error {
	query := `DELETE FROM "user" WHERE id = $1 RETURNING id`
	rows, err := ur.client.Query(query, &u.Id)
	if err != nil {
		return err
	}

	defer rows.Close()

	if !rows.Next() {
		u.Msg = "User doesn't exist"
		return nil
	}

	Row := models.SqlUser{}
	err = rows.Scan(&Row.Id)
	if err != nil {
		return err
	}

	return nil
}

func (ur *postgresUserRepository) SelectUser(u *models.GetUserInfo) error {

	var query string
	var val interface{}

	if u.Email != "" {
		query = `SELECT id, email, password, native_language, created_at, updated_at, deleted FROM "user" WHERE email = $1`
		val = u.Email
	}

	if u.Id != 0 {
		query = `SELECT id, email, password, native_language, created_at, updated_at, deleted FROM "user" WHERE id = $1`
		val = u.Id
	}

	if query == "" {
		return nil
	}

	rows, err := ur.client.Query(query, val)
	if err != nil {
		return err
	}
	defer rows.Close()

	if !rows.Next() {
		u.Msg = "User doesn't exist"
		return nil
	}

	err = rows.Scan(&u.SqlUser.Id, &u.SqlUser.Email, &u.SqlUser.Password, &u.SqlUser.NativeLanguage, &u.SqlUser.CreatedAt, &u.SqlUser.UpdatedAt, &u.SqlUser.Deleted)
	if err != nil {
		return err
	}

	return nil
}

//Получение информации для авторизации
func (ur *postgresUserRepository) SelectUserAuthorizationData(u *models.GetAccessInfo) error {
	query := `SELECT email, password, deleted FROM "user" WHERE email = $1`
	rows, err := ur.client.Query(query, &u.Email)
	if err != nil {
		return err
	}

	defer rows.Close()

	if !rows.Next() {
		u.Msg = "User doesn't exist"
		return nil
	}

	err = rows.Scan(&u.Email, &u.Password, &u.Deleted)
	if err != nil {
		return err
	}

	return nil

}

//Проверяет сущевтует ли user в бд
func (ur *postgresUserRepository) IsUserExistById(id int) bool {
	var query strings.Builder
	query.WriteString(`SELECT 1 FROM "user" WHERE id = `)
	query.WriteString(strconv.Itoa(id))
	query.WriteString(`LIMIT 1`)

	rows, _ := ur.client.Query(query.String())
	defer rows.Close()

	if !rows.Next() {
		return false
	}
	return true
}

func (ur *postgresUserRepository) IsUserExistByEmail(email string) bool {
	var query strings.Builder
	query.WriteString(`SELECT 1 FROM "user" WHERE email = '`)
	query.WriteString(email)
	query.WriteString(`' LIMIT 1`)

	rows, _ := ur.client.Query(query.String())
	defer rows.Close()

	if !rows.Next() {
		return false
	}
	return true
}
