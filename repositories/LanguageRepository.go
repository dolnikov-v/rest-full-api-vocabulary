package repositories

import (
	"bitbucket.org/dolnikov-v/go-lesson/app/database/postgres"
	"bitbucket.org/dolnikov-v/go-lesson/models"
)

type postgresLanguageRepository struct {
	client postgres.DBConnection
}

func NewLanguageRepository(db postgres.DBConnection) LanguageRepository {
	ar := postgresLanguageRepository{
		client: db,
	}
	return &ar
}

func (lr *postgresLanguageRepository) GetLanguage(limit uint16, offset uint16) ([]models.SqlLanguage, error) {

	query := `SELECT id, code, name, can_use_as_native FROM language LIMIT $1 OFFSET $2`

	rows, err := lr.client.Query(query, limit, offset)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var Languages []models.SqlLanguage
	for rows.Next() {
		Row := models.SqlLanguage{}
		err = rows.Scan(&Row.Id, &Row.Code, &Row.Name, &Row.CanUseAsNative)
		if err != nil {
			return nil, err
		}

		Languages = append(Languages, Row)
	}

	return Languages, nil
}
