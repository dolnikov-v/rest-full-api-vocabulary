package repositories

import (
	"bitbucket.org/dolnikov-v/go-lesson/models"
)

type LanguageRepository interface {
	GetLanguage(limit uint16, offset uint16) ([]models.SqlLanguage, error)
}
