package main

import (
	"bitbucket.org/dolnikov-v/go-lesson/app"
	"log"
)

func main() {

	application, err := app.NewApplication()
	if err != nil {
		log.Fatalf("Ошибка при запуске приложения: %s", err)
		return
	}
	log.Printf("Приложение успешно запущено! Порт: (%d)", application.Config.SystemSettings.Port)

	application.StopApplication()
}


