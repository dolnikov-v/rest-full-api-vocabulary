package controllers

import (
	"github.com/valyala/fasthttp"
)

type Base struct{}

func NewBaseController() *Base {
	c := &Base{}
	return c
}

//Запрос на получение статуса сервера
//Метод OPTIONS вызывается перед выполением любого запрса
//Чтобы api клиент мог убедиться что сервер доступен
func (b *Base) HandleOption(ctx *fasthttp.RequestCtx) {}
