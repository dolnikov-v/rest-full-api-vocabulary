package controllers

import (
	"bitbucket.org/dolnikov-v/go-lesson/app/config"
	"bitbucket.org/dolnikov-v/go-lesson/facades"
	Helper "bitbucket.org/dolnikov-v/go-lesson/helpers"
	"bitbucket.org/dolnikov-v/go-lesson/models"
	"encoding/json"
	"github.com/valyala/fasthttp"
	"strconv"
)

type User struct {
	Base
	config     *config.Config
	userFacade *facades.UserFacade
}

func NewUserController(config *config.Config, userFacade *facades.UserFacade) *User {
	u := &User{}
	u.config = config
	u.userFacade = userFacade
	return u
}

func (u *User) HandlePost(ctx *fasthttp.RequestCtx) {
	postUserRequest := new(models.PostUserRequest)
	err := json.Unmarshal(ctx.PostBody(), &postUserRequest)
	Helper.CheckError(err)

	if postUserRequest.Email == "" {
		Helper.ToError(ctx, 409, "There no email field")
		return
	}

	if postUserRequest.NativeLanguage == 0 {
		Helper.ToError(ctx, 409, "There no native_language field")
		return
	}

	if postUserRequest.Password == "" {
		Helper.ToError(ctx, 409, "There no password field")
		return
	}

	userData, err := u.userFacade.AddUser(*postUserRequest)
	Helper.CheckError(err)

	ctx.Response.SetBody(Helper.ToJson(userData))
}

func (u *User) HandleDelete(ctx *fasthttp.RequestCtx) {
	deleteUserRequest := new(models.DeleteUserRequest)
	err := json.Unmarshal(ctx.PostBody(), &deleteUserRequest)
	Helper.CheckError(err)

	if deleteUserRequest.Id == 0 {
		Helper.ToError(ctx, 409, "You need to add Id field")
		return
	}

	err = u.userFacade.DeleteUser(deleteUserRequest)
	Helper.CheckError(err)

	if deleteUserRequest.Msg != "" {
		Helper.ToError(ctx, 404, deleteUserRequest.Msg)
		return
	}

	ctx.Response.SetBody(Helper.ToJson(true))
}

func (u *User) HandleGetUserInfoByEmail(ctx *fasthttp.RequestCtx) {
	getUserInfo := new(models.GetUserInfo)
	AttributeEmail := ctx.UserValue(config.AttributeEmail).(string)

	if AttributeEmail == "" {
		Helper.ToError(ctx, 409, "There no email")
		return
	}

	getUserInfo.Email = AttributeEmail

	err := u.userFacade.GetUserInfo(getUserInfo)
	Helper.CheckError(err)

	if getUserInfo.Msg != "" {
		Helper.ToError(ctx, 404, getUserInfo.Msg)
		return
	}

	ctx.Response.SetBody(Helper.ToJson(getUserInfo.SqlUser))
}

func (u *User) HandleGet(ctx *fasthttp.RequestCtx) {
	getUserInfo := new(models.GetUserInfo)
	err := json.Unmarshal(ctx.PostBody(), &getUserInfo)
	Helper.CheckError(err)

	if getUserInfo.Id == 0 && getUserInfo.Email == "" {
		Helper.ToError(ctx, 409, "You need to add Id or Email field")
		return
	}

	err = u.userFacade.GetUserInfo(getUserInfo)
	Helper.CheckError(err)

	if getUserInfo.Msg != "" {
		Helper.ToError(ctx, 404, getUserInfo.Msg)
		return
	}

	ctx.Response.SetBody(Helper.ToJson(getUserInfo.SqlUser))
}

func (u *User) HandleGetExistByEmail(ctx *fasthttp.RequestCtx) {
	email := ctx.UserValue(config.AttributeEmail).(string)
	res := u.userFacade.CheckUserExistsByEmail(email)
	ctx.Response.SetBody(Helper.ToJson(res))
}

func (u *User) HandleGetExistById(ctx *fasthttp.RequestCtx) {
	idStr := ctx.UserValue(config.AttributeId).(string)
	idInt32, _ := strconv.ParseInt(idStr, 10, 0)
	id := int(idInt32)
	res := u.userFacade.CheckUserExistsById(id)
	ctx.Response.SetBody(Helper.ToJson(res))
}

func (u *User) HandleLoginPost(ctx *fasthttp.RequestCtx) {
	getAccessInfo := new(models.GetAccessInfo)
	err := json.Unmarshal(ctx.PostBody(), &getAccessInfo)
	Helper.CheckError(err)

	if getAccessInfo.Email == "" {
		Helper.ToError(ctx, 200, "There no email field")
		return
	}

	if getAccessInfo.Password == "" {
		Helper.ToError(ctx, 200, "There no password field")
		return
	}

	res, err := u.userFacade.UserLogin(getAccessInfo)
	Helper.CheckError(err)

	if getAccessInfo.Msg != "" {
		Helper.ToError(ctx, 200, getAccessInfo.Msg)
		return
	}

	if getAccessInfo.Deleted {
		Helper.ToError(ctx, 200, "User is deleted")
		return
	}

	tokens := Helper.GetAccessJWToken(getAccessInfo.Email)
	ctx.Response.Header.Set("access-token", tokens["access_token"])
	ctx.Response.Header.Set("refresh-token", tokens["refresh_token"])
	ctx.Response.SetBody(Helper.ToJson(res))

}

func (u *User) HandleLoginRefreshPost(ctx *fasthttp.RequestCtx) {
	auth := ctx.Request.Header.Peek("Authorization")

	//1) Распарсить auth
	//2) Проверить валидный ли токен
	//3) Если да, то сгенерировать новую пару ключей

}
