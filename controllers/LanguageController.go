package controllers

import (
	"bitbucket.org/dolnikov-v/go-lesson/app/config"
	"bitbucket.org/dolnikov-v/go-lesson/facades"
	"bitbucket.org/dolnikov-v/go-lesson/helpers"
	"bitbucket.org/dolnikov-v/go-lesson/models"
	"github.com/valyala/fasthttp"
	"strconv"
)

type Language struct {
	Base
	config         *config.Config
	languageFacade *facades.LanguageFacade
}

func NewLanguageController(config *config.Config, languageFacade *facades.LanguageFacade) *Language {
	c := &Language{}
	c.config = config
	c.languageFacade = languageFacade
	return c
}

//Получить список всех языков
func (l *Language) HandleGet(ctx *fasthttp.RequestCtx) {
	getLanguageRequest := new(models.GetLanguageRequest)

	//Получаем параметр limit
	limitStr := string(ctx.FormValue("limit"))
	limitUint64, _ := strconv.Atoi(limitStr)
	getLanguageRequest.Limit = uint16(limitUint64)

	//Получаем параметр offset
	offsetStr := string(ctx.FormValue("offset"))
	offsetUint64, _ := strconv.Atoi(offsetStr)
	getLanguageRequest.Offset = uint16(offsetUint64)

	languageData, err := l.languageFacade.GetInfo(getLanguageRequest.Limit, getLanguageRequest.Offset)
	Helper.CheckError(err)

	ctx.Response.SetBody(Helper.ToJson(languageData))
}
