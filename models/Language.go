package models

type Language struct {
	GetLanguageRequest
	Languages []SqlLanguage `json:"languages"`
}

type SqlLanguage struct {
	Id             int    `json:"id"`
	Code           string `json:"code"`
	Name           string `json:"name"`
	CanUseAsNative bool   `json:"can_use_as_native"`
}

type GetLanguageRequest struct {
	Limit  uint16 `json:"limit"`
	Offset uint16 `json:"offset"`
}
