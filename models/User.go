package models

type User struct {
	PostUserRequest
	User []SqlUser `json:"languages"`
}

type SqlUser struct {
	Id             int64  `json:"id"`
	Email          string `json:"email"`
	Password       string `json:"password"`
	NativeLanguage int    `json:"native_language"`
	CreatedAt      int    `json:"created_at"`
	UpdatedAt      int    `json:"updated_at"`
	Deleted        bool   `json:"deleted"`
}

type PostUserRequest struct {
	SqlUser
}

type DeleteUserRequest struct {
	Id  int64  `json:"id"`
	Msg string `json:"message"`
}

type GetUserInfo struct {
	Id    uint16 `json:"id"`
	Email string `json:"email"`
	Msg   string `json:"message"`
	SqlUser
}

type GetAccessInfo struct {
	Email    string `json:"email"`
	Password string `json:"password"`
	Deleted  bool   `json:"deleted"`
	Msg      string `json:"message"`
}
