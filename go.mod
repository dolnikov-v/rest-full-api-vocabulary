module bitbucket.org/dolnikov-v/go-lesson

go 1.12

require (
	github.com/codegangsta/negroni v1.0.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/fasthttp/router v0.3.3
	github.com/kr/pretty v0.1.0 // indirect
	github.com/lib/pq v1.2.0
	github.com/pelletier/go-toml v1.3.0
	github.com/pkg/errors v0.8.1
	github.com/rs/zerolog v1.13.0
	github.com/valyala/fasthttp v1.2.0
	golang.org/x/crypto v0.0.0-20190911031432-227b76d455e7
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
)
