package facades

import (
	"bitbucket.org/dolnikov-v/go-lesson/models"
	"bitbucket.org/dolnikov-v/go-lesson/repositories"
	"database/sql"
)

type LanguageFacade struct {
	db *sql.DB
}

func NewLanguageFacade(db *sql.DB) *LanguageFacade {
	f := new(LanguageFacade)
	f.db = db
	return f
}

func (f *LanguageFacade) GetInfo(limit uint16, offset uint16) (*models.Language, error) {

	language := new(models.Language)
	languageRepository := repositories.NewLanguageRepository(f.db)

	if limit == 0 {
		limit = 20
	}

	sqlLanguage, err := languageRepository.GetLanguage(limit, offset)

	if err != nil {
		return nil, err
	}
	if sqlLanguage == nil {
		return nil, nil
	}

	language.Languages = sqlLanguage
	language.Limit = limit
	language.Offset = offset
	return language, nil
}
