package facades

import (
	"bitbucket.org/dolnikov-v/go-lesson/helpers"
	"bitbucket.org/dolnikov-v/go-lesson/models"
	"bitbucket.org/dolnikov-v/go-lesson/repositories"
	"database/sql"
	"time"
)

type UserFacade struct {
	db *sql.DB
}

func NewUserFacade(db *sql.DB) *UserFacade {
	f := new(UserFacade)
	f.db = db
	return f
}

//Создание пользователя
func (u *UserFacade) AddUser(user models.PostUserRequest) (*models.SqlUser, error) {

	var err error
	user.SqlUser.Password, err = Helper.HashPassword(user.SqlUser.Password)
	if err != nil {
		return nil, err
	}

	timestamp := int(time.Now().Unix())
	user.SqlUser.CreatedAt = timestamp
	user.SqlUser.UpdatedAt = timestamp

	userRepository := repositories.NewUserRepository(u.db)
	sqlUser, err := userRepository.Save(&user.SqlUser)
	if err != nil {
		return nil, err
	}
	user.SqlUser = *sqlUser

	return sqlUser, nil
}

//Создание пользователя из бд
func (u *UserFacade) DeleteUser(user *models.DeleteUserRequest) error {

	userRepository := repositories.NewUserRepository(u.db)
	err := userRepository.DeleteByUserId(user)

	if err != nil {
		return err
	}

	return nil
}

//Получение информации о пользователе по id или email
func (u *UserFacade) GetUserInfo(user *models.GetUserInfo) error {
	userRepository := repositories.NewUserRepository(u.db)
	err := userRepository.SelectUser(user)
	if err != nil {
		return err
	}

	return nil
}

//Проверить существует ли пользователь по ID
func (u *UserFacade) CheckUserExistsById(id int) bool {
	userRepository := repositories.NewUserRepository(u.db)
	res := userRepository.IsUserExistById(id)
	return res
}

//Проверить существует ли пользователь по Email
func (u *UserFacade) CheckUserExistsByEmail(email string) bool {
	userRepository := repositories.NewUserRepository(u.db)
	res := userRepository.IsUserExistByEmail(email)
	return res
}

//Проверка доступа пользователя
func (u *UserFacade) UserLogin(data *models.GetAccessInfo) (bool, error) {
	userRepository := repositories.NewUserRepository(u.db)
	password := data.Password
	err := userRepository.SelectUserAuthorizationData(data)

	if err != nil {
		return false, err
	}

	if !Helper.CheckPasswordHash(password, data.Password) {
		data.Msg = "Неправильнвый пароль"
		return false, nil
	}

	return true, nil
}
