package Helper

import (
	"bitbucket.org/dolnikov-v/go-lesson/app/config"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"golang.org/x/crypto/bcrypt"
	"io/ioutil"
	"log"
	"time"
)

var (
	PublicKey  []byte
	PrivateKey []byte
)

//Инициализация shh ключей
func InitSshKeys() error {
	var err error

	PrivateKey, err = ioutil.ReadFile(config.PrivateKeyPath)
	if err != nil {
		return err
	}

	PublicKey, err = ioutil.ReadFile(config.PublicKeyPath)
	if err != nil {
		return err
	}

	return nil
}

//хеширование пароля
func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	return string(bytes), err
}

//Проверка соответствия пароля и хеша bcrypt
func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

//Герерация JWT токена для авторизации
func GetAccessJWToken(email string) map[string]string {
	//Access Token
	token := jwt.New(jwt.SigningMethodRS256)
	claims := make(jwt.MapClaims)
	claims["email"] = email
	claims["exp"] = time.Now().Add(time.Second * 30).Unix()
	token.Claims = claims

	key, err := jwt.ParseRSAPrivateKeyFromPEM(PrivateKey)
	if err != nil {
		log.Fatalf("Error private token: %v\n", err)
	}

	accessToken, err := token.SignedString(key)
	if err != nil {
		log.Fatal(err)
	}

	//Refresh Token
	rToken := jwt.New(jwt.SigningMethodRS256)
	rClaims := make(jwt.MapClaims)
	rClaims["exp"] = time.Now().Add(time.Hour * 8760).Unix()
	rToken.Claims = rClaims

	refreshToken, err := rToken.SignedString(key)
	if err != nil {
		log.Fatal(err)
	}

	return map[string]string{
		"access_token":  accessToken,
		"refresh_token": refreshToken,
	}
}

//Герерация Refresh JWT токена для авторизации
func GetRefreshJWToken() string {
	token := jwt.New(jwt.SigningMethodRS256)
	claims := make(jwt.MapClaims)
	claims["exp"] = time.Now().Add(time.Minute * 5).Unix()
	token.Claims = claims

	key, err := jwt.ParseRSAPrivateKeyFromPEM(PrivateKey)
	if err != nil {
		log.Fatalf("Error private token: %v\n", err)
	}

	tokenString, err := token.SignedString(key)
	if err != nil {
		log.Fatal(err)
	}

	return tokenString
}

//Проверка авторизации пользователя по ключу
func CheckJWToken(unparsedToken string) bool {
	key, err := jwt.ParseRSAPublicKeyFromPEM(PublicKey)
	if err != nil {
		log.Fatalf("Error parsing RSA public key: %v\n", err)
	}

	token, err := jwt.Parse(unparsedToken, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return key, nil
	})

	if err == nil && token.Valid {
		return true
	}

	return false
}
