package Helper

import "encoding/json"

//Преобразует в json
func ToJson(model interface{}) []byte {
	result, err := json.Marshal(model)
	CheckError(err)
	return result
}
