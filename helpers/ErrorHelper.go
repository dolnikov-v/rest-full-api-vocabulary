package Helper

import "github.com/valyala/fasthttp"

type ErrorResponse struct {
	Message string `json:"message"`
}

//Возвращает ошибку и код ошибки
//(ctx, 404, "Не задан пароль")
func ToError(ctx *fasthttp.RequestCtx, code int, message string) {
	ctx.SetStatusCode(code)
	ctx.SetBody(ToJson(&ErrorResponse{Message: message}))
}

//Вывод ошибки если она есть
func CheckError(err error) {
	if err != nil {
		panic(err)
	}
}
